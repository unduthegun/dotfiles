set -x LESS_TERMCAP_mb (printf "\e[01;31m")      # begin blinking
set -x LESS_TERMCAP_md (printf "\e[01;31m")      # begin bold
set -x LESS_TERMCAP_me (printf "\e[0m")          # end mode
set -x LESS_TERMCAP_se (printf "\e[0m")          # end standout-mode
set -x LESS_TERMCAP_so (printf "\e[01;44;33m")   # begin standout-mode - info box
set -x LESS_TERMCAP_ue (printf "\e[0m")          # end underline
set -x LESS_TERMCAP_us (printf "\e[01;32m")      # begin underline

set -x EDITOR "nvim"
set -x MANPAGER "nvim +Man!"

set -x XDG_CACHE_HOME "$HOME/.cache"
set -x XDG_CONFIG_HOME "$HOME/.config"
set -x XDG_DATA_HOME "$HOME/.local/share"
set -x GIMP2_DIRECTORY "$XDG_CONFIG_HOME/gimp"
set -x CCACHE_DIR "$XDG_CONFIG_HOME/ccache"
set -x LESSHISTFILE /dev/null

set -x XENPKGCONFIGDIR "$HOME"

# virtualfish
set -x VIRTUALFISH_HOME ~/.local/etc/virtualenvs

# go
set -x GOPATH ~/code/go

# ocaml
set -x OPAMROOT $XDG_DATA_HOME/opam

# rust
set -x RUSTUP_HOME $XDG_DATA_HOME/rustup
set -x CARGO_HOME $XDG_DATA_HOME/cargo

# ruby
set -x GEM_HOME ~/.local/share/gems
set -x GEM_PATH ~/.local/share/gems

function cp --wraps=cp --description 'alias cp=cp -i'
  command cp -i $argv
end
function mv --wraps=mv --description 'alias rm=rm -i'
  command mv -i $argv
end
function wget --wraps=wget --description 'alias wget=wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
  command wget --hsts-file="$XDG_CACHE_HOME/wget-hsts" $argv
end
function yarn --wraps=yarn --description 'alias yarn=yarn --use-yarnrc "$XDG_CONFIG_HOME/yarn/config"'
  command yarn --use-yarnrc "$XDG_CONFIG_HOME/yarn/config" $argv
end

# colors are cucumbers
function ls --wraps=ls
  command ls -h -F --color --group-directories-first $argv
end
function tree --wraps=tree
  command tree -C $argv
end

# clipboard
function toclip
  fish_clipboard_copy
end
function fromclip
  fish_clipboard_paste
end

if status --is-interactive
  abbr --add --global -- d dune
  abbr --add --global -- g git
  abbr --add --global -- o opam
  abbr --add --global -- p podman
  abbr --add --global -- vim nvim
  abbr --add --global -- wget curl -ROL
  abbr --add --global -- md mkdir -p
end

# force cat to print escape characters
alias cat "cat --show-nonprinting"

set fzf_diff_highlighter delta --paging=never --width=20
fzf_configure_bindings --directory=\cf --history=\cr --git_log=\cg

# hardcode user binary location when systemd is not available
set -l sysd_path (command -v systemd-path)
if test -z "$sysd_path"
  set localbin {$HOME}/.local/bin
else
  set localbin (systemd-path user-binaries)
end

# MacOS workaround to get proper coreutils
set -l uubin "/usr/local/opt/uutils-coreutils/libexec/uubin"
if test ! -d "{$uubin}"
 set -l uubin ""
end

# add some folders with binaries to user's path
for path in {$localbin} {$GOPATH}/bin {$CARGO_HOME}/bin {$uubin}
  contains $path $fish_user_paths; or set -a fish_user_paths $path
end

eval (dircolors -c ~/.config/dir_colors/sane_ntfs.dircolors)


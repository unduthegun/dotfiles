function git-sync
  set -l remotes $argv
  if test (count $remotes) -eq 0
    set -a remotes (git remote)
  end

  set -l original_ref (git rev-parse --abbrev-ref HEAD)

  for remote in $remotes
    git remote update "$remote" --prune

    for remote_local in (git remote show "$remote" -n | awk '/merges with remote/{print $5","$1}')
      echo $remote_local | read -d , remote_br local_br
      set -l remote_ref "refs/remotes/$remote/$remote_br"
      set -l local_ref "refs/heads/$local_br"

      set -l behind (math (git rev-list --count "$local_ref..$remote_ref" 2> /dev/null) + 0)
      set -l ahead (math (git rev-list --count "$remote_ref..$local_ref" 2> /dev/null) + 0)

      if test "$behind" -ne 0
        if test "$ahead" -ne 0
          echo " branch $local_br is $behind commit(s) behind and $ahead commit(s) ahead of $remote/$remote_br. could not be fast-forwarded"
        else if test "$local_br" = "$original_ref"
          echo "$local_br fast-forwarded"
          git merge -q "$remote_ref"
        else
          echo "$local_br reset"
          git branch -f "$local_br" -t "$remote_ref" > /dev/null
        end
      end
    end
  end
end

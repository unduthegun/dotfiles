function opam-new
  if test (count $argv) -lt 2
    echo "opam_new: expected a least 2 args"
    echo "Usage: oswitch <name> <version>"
    return -1
  end
  set name "$argv[1]"
  set ver "$argv[2]"
  set package "$name.$ver"
  mkdir -p "packages/$name/$package"
  opam show --raw "$package" > "packages/$name/$package/opam"
end

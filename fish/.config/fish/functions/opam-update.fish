function opam-update
  if test (count $argv) -ne 3
    echo "opam_update: expected 3 args"
    echo "Usage: oswitch <name> <old> <new>"
    return -1
  end
  set name "$argv[1]"
  set old "$argv[2]"
  set new "$argv[3]"
  git mv packages/$name/$name.$old packages/$name/$name.$new
  opam show --raw $name.$new > packages/$name/$name.$new/opam
end

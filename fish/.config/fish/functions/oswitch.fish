function oswitch
    if test (count $argv) -lt 1
        echo "oswitch: expected a least an arg, got none"
        echo "Usage: oswitch <switch>"
        return -1
    end
    if [ "$argv[1]" = '-' ]
      set cmd (opam switch - | grep -o '(.*)'| sed 's/[()]//g')
      if [ "$cmd" != "" ]
        eval (eval $cmd 2> /dev/null)
      end
    else
      set switch "$argv[1]"
      if opam switch set --dry-run "$switch" 2> /dev/null > /dev/null
          opam switch "$switch" > /dev/null
          eval (opam env --switch="$switch" --set-switch)
      else
          # show output
          opam switch set --dry-run "$switch"
      end
    end
end
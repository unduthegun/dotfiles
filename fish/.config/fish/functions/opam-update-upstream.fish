function opam-update-upstream
  opam repo add default
  opam repo priority default 1
  for dir in packages/*/*
    set -l versioned (path basename $dir)
    if string match --regex -v '.master' $versioned
      opam show --raw "$versioned" > "$dir/opam"
    end
  end
end

function getlog
  curl -RL "$argv[1]" > logs.tar.bz2
  unp logs.tar.bz2
  pushd logs
  for bugreport in **/bug-report*.tar.gz; pushd (dirname $bugreport); and unp (basename $bugreport); and popd; end
end

import XMonad

import XMonad.Config.Xfce

import XMonad.Layout.Minimize (minimize)
import XMonad.Layout.MouseResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile

import XMonad.Actions.CycleWS

import XMonad.Hooks.ManageDocks (avoidStruts, manageDocks)
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog, doFullFloat, doCenterFloat)
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.Minimize (minimizeEventHook)
import XMonad.Hooks.SetWMName

import XMonad.Util.EZConfig

import Data.Monoid (mappend)

import qualified XMonad.StackSet as W

-- colours
mBlack       = "#000000"
mBlackBright = "#393939"

mWhite       = "#c4c4c4"
mWhiteBright = "#ffffff"

mRed         = "#ac0028"
mRedBright   = "#c30233"

mBlue        = "#0078ad"
mBlueBright  = "#0086db"

-- font used for tabs
mFont = "xft:Source Sans Pro Medium-12"

-- layouts
mLayout = tile ||| tab ||| full
  where
    tile    = smartBorders
            $ minimize
            $ avoidStruts
            $ mouseResizableTile
                    { nmaster       = 1
                    , fracIncrement = (3/100)
                    , masterFrac    = (6/11)
                    }

    -- fullscreen with tabs
    tab     = noBorders $ tabbed shrinkText mTabTheme

    -- fullscreen without tabs
    full    = noBorders Full

-- workspaces
mWorkspaces = map show ([1 .. 9 :: Int] ++ [0 :: Int])

-- window manager
mWManager :: ManageHook
mWManager = composeAll . concat $
            [ [manageHook xfceConfig]
            , [manageDocks]
            -- windows that should be sent to a workspace
            , [className =? "Gajim"       --> doShift "2"]
            , [className =? "Slack"       --> doShift "2"]
            , [className =? "Thunderbird" --> doShift "3"]
            , [className =? "mpv"         --> doShift "4"]
            , [className =? "quodlibet"   --> doShift "5"]
            -- windows that shouldn't be tiled
            , [isDialog                   --> doFloat]
            , [className =? cln           --> doFloat | cln <- classFloats]
            , [title     =? ttl           --> doFloat | ttl <- titleFloats]
            -- windows to ignore
            , [className =? cln           --> doIgnore | cln <- classIgnore]
            -- make fullscreen apps really fullscreen
            , [isFullscreen --> doFullFloat]
            ]
              where
                classFloats = [ "xfce4-settings-manager"
                              , "xfce4-appfinder"
                              , "xfce4-panel"
                              , "pavucontrol"
                              , "wicd-client.py"
                              , "wrapper"
                              , "yakuake"
                              ]
                titleFloats = [ "File Operation Progress"
                              , "About Mozilla Firefox"
                              , "Add New Items"
                              , "Application Finder"
                              ]
                classIgnore = [ "Conky"
                              , "xfce4-notifyd"
                              ]

-- keybindings
mKeyBindings = [ ("M-<Tab>",      windows W.focusDown)
               -- M1 is alt
               , ("M-M1-<Left>",  windows W.focusDown)
               , ("M-M1-<Right>", windows W.focusUp)
               -- expand window (v)
               , ("M-w",          sendMessage ExpandSlave)
               -- shrink window (v)
               , ("M-s",          sendMessage ShrinkSlave)
               -- expand window (h)
               , ("M-f",          sendMessage Expand)
               -- shrink window (h)
               , ("M-d",          sendMessage Shrink)
               -- switch to last workspace
               , ("M-a",          toggleWS)
               , ("M-<Right>",    nextWS)
               , ("M-<Left>",     prevWS)
               , ("M-<Up>",       nextWS)
               , ("M-<Down>",     prevWS)
               , ("M-S-<Right>",  shiftToNext)
               , ("M-S-<Left>" ,  shiftToPrev)
               , ("M-S-l",        spawn "light-locker-command -l")
               ]

-- terminal
mTerm = "konsole"

-- keybindings
mMask = mod4Mask

-- events
mEvents = fullscreenEventHook <+> handleEventHook xfceConfig <+> ewmhDesktopsEventHook `mappend` minimizeEventHook

-- tabs
mTabTheme = defaultTheme
          { activeColor           = mWhiteBright
          , inactiveColor         = mWhite
          , urgentColor           = mWhite
          , activeBorderColor     = mBlueBright
          , inactiveBorderColor   = mWhite
          , urgentBorderColor     = mRedBright
          , activeTextColor       = mBlackBright
          , inactiveTextColor     = mBlackBright
          , urgentTextColor       = mRed
          , decoHeight            = 20
          , fontName              = mFont
          }

-- borderWidth
mBorderWidth = 3

-- whole configuration
mConfig = ewmh xfceConfig
         { modMask            = mMask
         , terminal           = mTerm
         , manageHook         = mWManager
         , layoutHook         = mLayout
         , workspaces         = mWorkspaces
         , focusedBorderColor = mBlue
         , normalBorderColor  = mWhite
         , borderWidth        = mBorderWidth
         , handleEventHook    = mEvents
         }

-- apply the custom settings
main :: IO ()
main = xmonad $ mConfig
  -- Identify as another window manager, because Java sucks
  { startupHook = startupHook mConfig >> setWMName "LG3D"}
  `additionalKeysP` mKeyBindings

[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Natural
Font=Iosevka Custom,12,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[Encoding Options]
DefaultEncoding=UTF-8

[General]
Environment=NVIM_TUI_ENABLE_TRUE_COLOR=1,
Icon=terminal
LocalTabTitleFormat=%d : %n
Name=default
Parent=FALLBACK/
RemoteTabTitleFormat=%d : %n (%u @ %H)

[Interaction Options]
TrimTrailingSpacesInSelectedText=true

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2
ScrollBarPosition=1

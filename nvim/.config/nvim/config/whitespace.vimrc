" function to remove trailing whitespaces
fun! <SID>StripTrailingWhitespaces()
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  call cursor(l, c)
endfun

" Can be used to remove trailing whitespace on save for a type:
" autocmd BufWritePre *.html :call <SID>StripTrailingWhitespaces()

nnoremap <Leader>w :call <SID>StripTrailingWhitespaces()<CR>

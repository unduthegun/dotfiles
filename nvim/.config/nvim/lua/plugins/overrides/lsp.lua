local M = {}

M.config = function ()
  vim.diagnostic.config({
    virtual_text = false,
    float = {
      source = 'always',
      focusable = false,   -- See neovim#16425
    },
  })

  vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float)
  vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist)
  vim.keymap.set('n', '<leader>N', vim.diagnostic.goto_prev)
  vim.keymap.set('n', '<leader>n', vim.diagnostic.goto_next)

  local on_attach = function(client, bufnr)
    local function set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local function set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    -- Always use signcolumn for the current buffer
    vim.wo.signcolumn = 'yes:1'

    set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
    set_option('tagfunc', 'v:lua.vim.lsp.tagfunc')

    -- Show lightbulbs
    require('nvim-lightbulb').setup({autocmd = {enabled = true}})

    -- Keybindings for LSPs
    -- Note these are in on_attach so that they don't override bindings in a non-LSP setting
    local opts = { noremap=true, silent=true }
    set_keymap("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
    set_keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
    set_keymap("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
    set_keymap("n", "<c-k>", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
    set_keymap("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
    set_keymap("n", "g0", "<cmd>lua vim.lsp.buf.document_symbol()<CR>", opts)
    set_keymap("n", "gW", "<cmd>lua vim.lsp.buf.workspace_symbol()<CR>", opts)
    set_keymap("n", "rn", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
    set_keymap("n", "re", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
  end

  local capabilities = require('cmp_nvim_lsp').default_capabilities()

  local lspconfig = require 'lspconfig'

  lspconfig.ocamllsp.setup({
    on_attach = on_attach,
    capabilities = capabilities,
    offset_encoding = "utf-8",
  })
  lspconfig.rust_analyzer.setup({
    on_attach = on_attach,
    capabilities = capabilities,
    offset_encoding = "utf-8",
  })

  local function sign(...) vim.fn.sign_define(...) end

  sign('LightBulbSign', { text = "⸰", texthl = "", linehl="", numhl="" })
  sign('DiagnosticSignError', { text = "×", texthl = "DiagnosticSignError", linehl="", numhl="" })
  sign('DiagnosticSignWarn', { text = "‼", texthl = "DiagnosticSignWarn", linehl="", numhl="" })
  sign('DiagnosticSignInfo', { text = "‽", texthl = "DiagnosticSignInfo", linehl="", numhl="" })
  sign('DiagnosticSignHint', { text = "⁇", texthl = "DiagnosticSignHint", linehl="", numhl="" })
end

return M

return {
  'nvim-treesitter/nvim-treesitter',
  build = ':TSUpdate',
  config = function()
    local configs = require("nvim-treesitter.configs")
    configs.setup({
      playground = {
        enable = true,
        disable = {},
        updatetime = 25
      },
      ensure_installed = "all",
      highlight = {
        enable = true,
        use_languagetree = true
      },
    })
  end,
  init = function()
    -- folding
    vim.opt.foldmethod = "expr"
    vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
    vim.opt.foldtext = "v:lua.vim.treesitter.foldtext()"
  end,
}

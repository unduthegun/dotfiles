return {
-- language support
'kosayoda/nvim-lightbulb',
{ 'undu/vim-ocaml',
  branch = 'ocaml_interface',
  ft = {'ocaml', 'ocamlinterface', 'ocamllex', 'menhir', 'dune', 'opam', 'oasis', 'omake', 'ocamlbuild_tags', 'sexplib'}
},

-- version control system
'mhinz/vim-signify',

-- eyecandy
'undu/numbers.vim',
'albfan/AnsiEsc.vim',
'undu/gruvbox',
}

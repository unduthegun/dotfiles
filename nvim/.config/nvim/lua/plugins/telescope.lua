return {
  'nvim-telescope/telescope.nvim', tag = '0.1.8',
  dependencies = { 'nvim-lua/plenary.nvim' },
  keys = {
    { "<leader>sf", "<cmd>Telescope find_files<cr>", desc = "Telescope search files" },
    { "<leader>sg", "<cmd>Telescope live_grep<cr>", desc = "Telescope search with grep" },
    { "<leader>so", "<cmd>Telescope current_buffer_fuzzy_find<cr>", desc = "Telescope search opened file" },
    { "<leader>sb", "<cmd>Telescope buffers<cr>", desc = "Telescope search buffers" },
    { "<leader>ss", "<cmd>Telescope spell_suggest<cr>", desc = "Telescope search spellings" },
    { "<leader>sh", "<cmd>Telescope help_tags<cr>", desc = "Telescope search help" },
  },
  opts = {
    defaults = {
      layout_strategy = 'flex',
      layout_config = {
        flex = { flip_columns = 200 },
        vertical = { width = 0.9 }
      },
    },
  },
}

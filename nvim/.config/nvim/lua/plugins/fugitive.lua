return {
  { 'tpope/vim-fugitive',
    event = "VeryLazy",
    keys = {
      { "<leader>gb", "<cmd>:Git blame -w<cr>", desc = "Git blame" },
      { "<leader>gl", "<cmd>:Git log<cr>", desc = "Git log" },
      { "<leader>gr", "<cmd>:Gread<cr>", desc = "Git read" },
      { "<leader>ge", "<cmd>:Gedit<cr>", desc = "Git edit" },
      { "<leader>gi", "<cmd>:Git add -p %<cr>", desc = "Git patch (interactive)" },
    },
  }
}

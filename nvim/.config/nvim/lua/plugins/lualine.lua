-- let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
--
-- let g:airline#extensions#whitespace#enabled = 1
-- let g:airline#extensions#whitespace#checks = [ 'mixed-indent-file', 'trailing' ]

branch = {'branch', icons_enabled = true}
diagnostics = {
  'diagnostics',
  symbols = {error = '×', warn = '‼', info= '‽', hint= '⁇'}
}
filename = {
  'filename',
  path = 1 -- relative path
}
lines = { "%L", separator = '☰'}

return {
  'nvim-lualine/lualine.nvim',
  event = 'VeryLazy',
  opts = {
    options = {
      icons_enabled = false,
    },
    sections = {
      lualine_a = {'mode'},
      lualine_b = { branch, 'diff', diagnostics},
      lualine_c = {filename},
      lualine_x = {'filetype'},
      lualine_y = {'searchcount', 'selectioncount'},
      lualine_z = {lines, 'location'}
    },
    inactive_sections = {
      lualine_c = {filename},
      lualine_x = {'filetype'},
      lualine_z = {lines, 'location'}
    },
    tabline = {
      lualine_a = {'buffers'}
    },
    extensions = {'quickfix'}
  }
}

vim.api.nvim_exec(
[[
autocmd BufRead,BufNewFile *.txt setlocal spell
autocmd FileType gitcommit setlocal spell
autocmd FileType markdown setlocal spell
autocmd FileType rst setlocal spell
autocmd FileType tex setlocal spell
]], false)

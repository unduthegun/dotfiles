-- disable unused providers
vim.g.loaded_node_provider = 0
vim.g.loaded_perl_provider = 0
vim.g.loaded_ruby_provider = 0

-- pick the python path regardless of the active virtualenv
vim.g.python3_host_prog = '/usr/bin/python3'

-- Change leader to space
vim.g.mapleader = ' '

-- Load plugins
require('config.lazy')

vim.cmd [[
runtime config/whitespace.vimrc
]]

require("spellcheck")

-- colorscheme
vim.o.termguicolors = true
vim.g.gruvbox_italic = 1
vim.g.gruvbox_contrast_dark = 'hard'
vim.g.gruvbox_invert_selection = 0
vim.g.gruvbox_sign_column = 'bg0'
vim.cmd [[ colorscheme gruvbox ]]

vim.cmd [[
" separators
hi! link Delimiter GruvboxOrange
hi! link Bracket GruvboxFg3

hi! link @punctuation.delimiter Delimiter
hi! link @punctuation.bracket Bracket

hi! link @string.regex String
hi! link @string.escape Constant
hi! link @string.special PreProc

hi! link @character.special PreProc

hi! link @function.call Function
hi! link @function.builtin Special

hi! link @method Function
hi! link @method.call Function

hi! link @constructor Special
hi! link @parameter Identifier

hi! link @keyword.function Keyword
hi! link @keyword.operator Keyword
hi! link @keyword.return Keyword

hi! link @type.builtin Type
hi! link @type.qualifier Type
hi! link @type.definition TypeDef

hi! link @attribute PreProc
hi! link @field Identifier
hi! link @property Identifier

hi! link @variable.builtin Special

hi! link @constant.builtin Type
hi! link @constant.macro Define

hi! link @namespace Include
hi! link @symbol Identifier

hi! link @text.environment Macro
hi! link @text.environment.name Type
hi! link @text.reference Constant

hi! link @text.note SpecialComment
hi! link @text.warning WarningMsg

hi! link @tag.attribute Identifier
]]

vim.o.pumblend = 20

vim.o.expandtab = true
vim.o.linebreak = true

vim.cmd [[ set nofoldenable ]]

-- copy to clipboard
vim.opt.clipboard:append { "unnamed" }

-- disable mouse crap
vim.cmd [[ set mouse= ]]

-- search
vim.o.ignorecase = true
vim.o.smartcase = true

-- completion options
vim.opt.completeopt = 'menu,menuone,preview,noselect'

-- substitution preview
vim.o.inccommand = "nosplit"

-- status
vim.o.showcmd = true

-- Better window navigation
--             <mode>  <keys>  <actions>
vim.keymap.set('n',    'C-h',  '<C-w>h')
vim.keymap.set('n',    'C-j',  '<C-w>j')
vim.keymap.set('n',    'C-k',  '<C-w>k')
vim.keymap.set('n',    'C-l',  '<C-w>l')

-- Exit the terminal with escape, as normal
vim.keymap.set('t', '<Esc>', "<C-\\><C-n>", {silent = true})

vim.cmd([[
" make n always search forward and N backward
nnoremap <expr> n 'Nn'[v:searchforward]
nnoremap <expr> N 'nN'[v:searchforward]

" make ; always "find" forward and , backward
nnoremap <expr> ; getcharsearch().forward ? ';' : ','
nnoremap <expr> , getcharsearch().forward ? ',' : ';'

set cursorline
set number

set splitbelow

set timeoutlen=500

let g:completion_enable_auto_popup = 0

" Prioritize git over other vcs in the gutter
let g:signify_vcs_list    = [ 'git', 'hg' ]

let g:signify_sign_change = '▏'
let g:signify_sign_delete = '▁'
let g:signify_sign_add = ''

" numbers don't belong in these buffers
let g:numbers_exclude = ['tagbar', 'nerdtree', 'man', '-stdin-', 'Help']

" exclude fugitive from editorconfig formatting
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" clear search pattern
command C let @/=""

" Make terminal mode more natural to get out of
tnoremap <Leader><Esc> <C-\><C-n>

" hard-wrap manpages
let g:man_hardwrap = 1

" persistent undo
set undofile

" toggle visible whitespaces
set listchars=tab:→\ ,trail:~,space:·,eol:¶,extends:>,precedes:<
noremap <F10> :set list!<CR>

" workaround in order to acomplish on-demand plugin loading
let g:rooter_disable_map = 1
map <silent> <unique> <Leader>cd <Plug>RooterChangeToRootDirectory

" Open files as readonly when they're already being edited
augroup NoSimultaneousEdits
  autocmd!
  autocmd SwapExists * let v:swapchoice = 'o'
  autocmd SwapExists * echohl ErrorMsg
  autocmd SwapExists * echomsg 'File is already open in another session'
  autocmd SwapExists * echohl None
augroup END
]])
